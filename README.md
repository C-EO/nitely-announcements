# nitely-announcements
- The `messages` file contains what's displayed for all nitely installations going forward.  
- The `donate_message` file is used when displaying the donate message.

This repo is separated from the main nitely repo to prevent unnecessary `updater` prompts.
